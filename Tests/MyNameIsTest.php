<?php

class MyNameIsTest extends PHPUnit_Framework_TestCase
{

    public function setUp()
    {
    // Demo Test
    }

    public function tearDown()
    {
    // Demo Test
    }

    public function testHelloOk()
    {
        // Test 1
        $Obj = new MyNameIs('Sergio');
        $this->assertEquals('Hello, My Name is: Sergio', $Obj->hello());
        
        // Test 2
        $Obj->setName('EOM Design Group');
        $this->assertEquals('Hello, My Name is: EOM Design Group', $Obj->hello());
    }

    public function testHelloError()
    {
        // Test 1 OK Error
        $Obj = new MyNameIs('EOM');
        $this->assertNotEquals('Hello, My Name is: empyt bla blaa...', $Obj->hello());
        
        // Test 2 OK Error
        $Obj->setName('blaaaaaaaaaaaaaa');
        $this->assertNotEquals('Hello, My Name is: empyt bla blaa...', $Obj->hello());
    }

}