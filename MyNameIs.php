<?php

class MyNameIs
{

    private $name = '';

    public function __construct($name = '')
    {
        $this->name = $name;
    }

    public function hello()
    {
        return "Hello, My Name is: ". $this->name;
    }


    public function setName($name = '')
    {
        return $this->name = $name;
    }
}